
# Changelog for SBD Upload and Share Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.3.0]

- maven-portal-bom 4.0.0
- maven-parent 1.2.0
- replaced social-networking-library with social-library-stubs

## [v1.2.0] - 2021-12-17

- Task #22596 Ported to git and removed Home Library dep.

## [v1.0.0] - 2019-05-21

First Release

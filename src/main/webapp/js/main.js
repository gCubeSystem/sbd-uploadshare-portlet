//open the url in a liferay popup
function showWPPopup(url) {
	var url = url;
	// this is one way of calling a pop-up in liferay
	// this way is specific to liferay
	Liferay.Util.openWindow(
			{
				dialog: {
					cache: false,
					width:700,
					height: 550,
					modal: true
				},
				id: 'testPopupIdUnique', 
				title: 'Select the dataset',
				uri: url
			}
	);
}


//handle the event from ws explorer (not used anymore)
Liferay.on('getSelectedItem', function(event) {
	var id = event.id;
	var name = event.name;
	var chip = $('<span class="badge badge-success" style="margin: 5px; padding:0 5px; line-height: 30px;">'+
			event.name + ' <button onClick="removeFromParent(this);" class="close">&times;</button>'+
			'<input type="hidden" name="selected-attachment" value="'+event.id+'"></span>');
	$("#attachedFiles").append(chip);
});

function appendChip(name, id) {
	var chip = $('<span class="badge badge-success" style="margin: 5px; padding:0 5px; line-height: 30px;">'+
			name + ' <button onClick="removeFromParent(this);" class="close">&times;</button>'+
			'<input type="hidden" name="selected-attachment" value="'+id+'"></span>');
	$("#attachedFiles").append(chip);
}

function removeFromParent(el) {	
	$(el).parent().remove();
}

function switchView(hideel, showel) {
	$(hideel).hide();
	$(showel).show();
}

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@page import="com.liferay.portal.model.User"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL var="uploadFileURL" id="uploadFiles" />
<% User currentUser = themeDisplay.getUser();  %>
<%
	if (SessionErrors.contains(renderRequest.getPortletSession(), "experiment-form-error")) {
%>
<liferay-ui:error key="experiment-form-error"
	message="Request VRE form submit had an issue, Please try again." />
<%
	}
%>

<portlet:actionURL var="addIdeaActionURL" windowState="maximized"
	name="addIdea">
</portlet:actionURL>

<portlet:renderURL var="maximizedState"
	windowState="<%=LiferayWindowState.MAXIMIZED.toString()%>" />
<portlet:renderURL var="normalState"
	windowState="<%=LiferayWindowState.NORMAL.toString()%>" />


<c:set var="maximised" scope="session"
	value="${renderRequest.getWindowState().toString().equalsIgnoreCase('maximized')}" />

<c:choose>
	<c:when test="${not maximised}">
		<div style="width: 100%; text-align: center; color: #3B5998;">
			<a class="btn btn-link" href="${maximizedState}"><i
				class="icon icon-bar-chart icon-3x"></i></a>
		</div>
	</c:when>
	<c:otherwise>
			<script
			src="<%=request.getContextPath()%>/js/jquery.uploadfile.min.js"></script>
		<script>
  		$(document).ready(function(){
	  	$("#multipleupload").uploadFile({
	      url:"<%=uploadFileURL.toString()%>",
					multiple : true,
					dragDrop : true,
					sequential : true,
					sequentialCount : 1,
					maxFileCount : 3,
					maxFileSize: 10*1024*1024,
					showStatusAfterSuccess: false,
					fileName : "myfile",
					onSuccess : function(files, data, xhr, pd) {
						var content = JSON.parse(data);
						appendChip(content.fileName, content.itemId);
					}
				});
			});
		</script>
		<a class="btn btn-link  btn-large" href="${normalState}"><i
			class="icon icon-angle-left"></i>&nbsp;Back (Cancel)</a>
		<p style="font-size: 15px; padding: 15px;">Please give us a
			description about the Virtual Research Environment (VRE) you wish to
			create. The project research, technical and ethical boards will
			evaluate your application and contact you about the result as soon as possible and always within 5 working days.</p>
		<div
			style="width: 100%; text-align: left; padding: 10px; color: #3B5998;">
			<aui:form action="<%=addIdeaActionURL%>" method="post" name="fm">
				<aui:input name="vre_name" label="VRE name" value="" required="true" />
				<aui:input type="textarea" rows="5" name="abstract"
					label="Abstract, 150 to 250 words" value="" required="true"
					style="width:90%;" />
				<aui:input name="vre-manager-name"
					label="Responsible person(s)" value="<%=currentUser.getFullName()%>"
					required="true" style="width:90%;" />
				<aui:input name="email" value="<%=currentUser.getEmailAddress()%>" label="Contact email (where to reach you)"> 
					<aui:validator name="required" />
					<aui:validator name="email" />
				</aui:input>
				<span>Logo and Additional Material (max 3 files, allowed maxsize per file: 10 MB):</span>
				<div id="multipleupload">Select file to attach</div>
				<div id="attachedFiles"></div>

				<aui:button-row>
					<a class="btn" href="${normalState}"><i class="icon-remove"></i>
						Cancel</a>
					<aui:button type="submit" value="Submit" />
				</aui:button-row>
			</aui:form>
		</div>
	</c:otherwise>

</c:choose>




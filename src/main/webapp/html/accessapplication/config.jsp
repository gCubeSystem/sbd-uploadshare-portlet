<%@include file="init.jsp"%>

<liferay-portlet:actionURL portletConfiguration="true"
	var="configurationURL" />

Here you can customise the icon to show besides the application name

<%
	String icondocURL = GetterUtil.getString(portletPreferences.getValue("icondocURL", StringPool.BLANK));

	String applicationName1Title = GetterUtil.getString(portletPreferences.getValue("applicationName1-Title", StringPool.BLANK));
	String applicationName2Title = GetterUtil.getString(portletPreferences.getValue("applicationName2-Title", StringPool.BLANK));
	String applicationName3Title = GetterUtil.getString(portletPreferences.getValue("applicationName3-Title", StringPool.BLANK));
	String applicationName4Title = GetterUtil.getString(portletPreferences.getValue("applicationName4-Title", StringPool.BLANK));

	long applicationName1DocumentId = GetterUtil.getLong(portletPreferences.getValue("applicationName1-DocumentId", StringPool.BLANK));
	long applicationName2DocumentId = GetterUtil.getLong(portletPreferences.getValue("applicationName2-DocumentId", StringPool.BLANK));
	long applicationName3DocumentId = GetterUtil.getLong(portletPreferences.getValue("applicationName3-DocumentId", StringPool.BLANK));
	long applicationName4DocumentId = GetterUtil.getLong(portletPreferences.getValue("applicationName4-DocumentId", StringPool.BLANK));

	String application1Url = GetterUtil.getString(portletPreferences.getValue("application1-Url", StringPool.BLANK));
	String application2Url = GetterUtil.getString(portletPreferences.getValue("application2-Url", StringPool.BLANK));
	String application3Url = GetterUtil.getString(portletPreferences.getValue("application3-Url", StringPool.BLANK));
	String application4Url = GetterUtil.getString(portletPreferences.getValue("application4-Url", StringPool.BLANK));

	
	String displayName_cfg = "";
%>

<aui:form action="<%=configurationURL%>" method="post" name="fm">
	<aui:input name="<%=Constants.CMD%>" type="hidden"
		value="<%=Constants.UPDATE%>" />
	<aui:input name="preferences--icondocURL--" type="text"
		cssClass="text long-field" showRequiredLabel="true" label="Icon docId"
		inlineField="true" inlineLabel="left" placeholder="Relative URL of the image"
		helpMessage="Relative URL of the image taken from Liferay CMS of the icons"
		value="<%=icondocURL%>" required="false">
		
	</aui:input>
	<p>Enter the application in the following:</p>
	<!-- Application URL -->
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
				<td><aui:input name="preferences--applicationName1-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="application Name 1" inlineField="true" inlineLabel="left"
						placeholder="application"
						helpMessage="Display Name of the application"
						value="<%=applicationName1Title%>" required="false" /></td>
				<td><aui:input
						name="preferences--applicationName1-DocumentId--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="application Document Id 1" inlineField="true"
						inlineLabel="left" placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=applicationName1DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--application1-Url--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 1" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=application1Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
				<td><aui:input name="preferences--applicationName2-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="application Name 2" inlineField="true" inlineLabel="left"
						placeholder="application"
						helpMessage="Display Name of the application"
						value="<%=applicationName2Title%>" required="false" /></td>
				<td><aui:input
						name="preferences--applicationName2-DocumentId--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="application Document Id 2" inlineField="true"
						inlineLabel="left" placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=applicationName2DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--application2-Url--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 2" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=application2Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
				<td><aui:input name="preferences--applicationName3-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="application Name 3" inlineField="true" inlineLabel="left"
						placeholder="application"
						helpMessage="Display Name of the application"
						value="<%=applicationName3Title%>" required="false" /></td>
				<td><aui:input
						name="preferences--applicationName3-DocumentId--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="application Document Id 3" inlineField="true"
						inlineLabel="left" placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=applicationName3DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--application3-Url--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 3" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=application3Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
				<td><aui:input name="preferences--applicationName4-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="application Name 4" inlineField="true" inlineLabel="left"
						placeholder="application"
						helpMessage="Display Name of the application"
						value="<%=applicationName4Title%>" required="false" /></td>
				<td><aui:input
						name="preferences--applicationName4-DocumentId--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="application Document Id 4" inlineField="true"
						inlineLabel="left" placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=applicationName4DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--application4-Url--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 4" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=application4Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>
</aui:form>
<%@include file="init.jsp"%>
<%@ page import="org.gcube.portlets.user.sbd.Utils" %>
<%@ page import="com.liferay.portal.model.User" %>
<%@ page import="com.liferay.portal.util.PortalUtil" %>

<%
	long groupId = themeDisplay.getLayout().getGroupId();
	User currentUser = PortalUtil.getUser( request );
	pageContext.setAttribute("icondocURL",
			GetterUtil.getString(portletPreferences.getValue("icondocURL", StringPool.BLANK)));

	pageContext.setAttribute("applicationName1Title",
			GetterUtil.getString(portletPreferences.getValue("applicationName1-Title", StringPool.BLANK)));
	pageContext.setAttribute("applicationName2Title",
			GetterUtil.getString(portletPreferences.getValue("applicationName2-Title", StringPool.BLANK)));
	pageContext.setAttribute("applicationName3Title",
			GetterUtil.getString(portletPreferences.getValue("applicationName3-Title", StringPool.BLANK)));
	pageContext.setAttribute("applicationName4Title",
			GetterUtil.getString(portletPreferences.getValue("applicationName4-Title", StringPool.BLANK)));

	long applicationName1DocumentId = GetterUtil
			.getLong(portletPreferences.getValue("applicationName1-DocumentId", StringPool.BLANK));
	long applicationName2DocumentId = GetterUtil
			.getLong(portletPreferences.getValue("applicationName2-DocumentId", StringPool.BLANK));
	long applicationName3DocumentId = GetterUtil
			.getLong(portletPreferences.getValue("applicationName3-DocumentId", StringPool.BLANK));
	long applicationName4DocumentId = GetterUtil
			.getLong(portletPreferences.getValue("applicationName4-DocumentId", StringPool.BLANK));

	
	String url1 = GetterUtil.getString(portletPreferences.getValue("application1-Url", StringPool.BLANK));
	pageContext.setAttribute("application1Url", Utils.getVREFriendlyURL(currentUser, url1));

	String url2 = GetterUtil.getString(portletPreferences.getValue("application2-Url", StringPool.BLANK));	
	pageContext.setAttribute("application2Url", Utils.getVREFriendlyURL(currentUser, url2));
	
	String url3 = GetterUtil.getString(portletPreferences.getValue("application3-Url", StringPool.BLANK));	
	pageContext.setAttribute("application3Url", Utils.getVREFriendlyURL(currentUser, url3));
	
	String url4 = GetterUtil.getString(portletPreferences.getValue("application4-Url", StringPool.BLANK));	
	pageContext.setAttribute("application4Url", url4);

	String content1 = "";
	if (applicationName1DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + applicationName1DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content1 = node.getText();
	}

	String content2 = "";
	if (applicationName2DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + applicationName2DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content2 = node.getText();
	}

	String content3 = "";
	if (applicationName3DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + applicationName3DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content3 = node.getText();
	}
	String content4 = "";
	if (applicationName4DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + applicationName4DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content4 = node.getText();
	}
%>
<c:if test="${not empty applicationName1Title}">
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${application1Url}"><img alt="" src="${icondocURL}">
				${applicationName1Title}</a>
		</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<span id="asset-app-summarypre1"> <%
 	if (content1.length() > 225)
 			out.println(HtmlUtil.stripHtml(content1.substring(0, 222) + " ..."));
 %>
					<div>
						<a
							href="javascript:switchView('#asset-app-summarypre1', '#asset-app-summary-1');">Read
							More � </a>
					</div>
				</span>
				<div class="asset-more">
					<div id="asset-app-summary-1" class="asset-summary"
						style="display: none;"><%=content1%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>
<c:if test="${not empty applicationName2Title}">
<!--  SECOND  -->
<div class="asset-abstract">
	<h3 class="asset-title">
		<a href="${application2Url}"><img alt="" src="${icondocURL}">
			${applicationName2Title}</a>
	</h3>
	<div class="asset-content">
		<div class="asset-summary" style="margin-left: 25px;">
			<span id="asset-app-summarypre2"> <%
 	if (content2.length() > 225)
 		out.println(HtmlUtil.stripHtml(content2.substring(0, 222) + " ..."));
 %>
				<div>
					<a
						href="javascript:switchView('#asset-app-summarypre2', '#asset-app-summary-2');">Read
						More � </a>
				</div>
			</span>
			<div class="asset-more">
				<div id="asset-app-summary-2" class="asset-summary"
					style="display: none;"><%=content2%></div>
			</div>
		</div>
		<div class="asset-metadata"></div>
	</div>
</div>
</c:if>
<c:if test="${not empty applicationName3Title}">
	<!--  THIRD  -->
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${application3Url}"><img alt="" src="${icondocURL}">
				${applicationName3Title}</a>
		</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<span id="asset-app-summarypre3"> <%
 	if (content3.length() > 225)
 		out.println(HtmlUtil.stripHtml(content3.substring(0, 222) + " ..."));
 %>
					<div>
						<a
							href="javascript:switchView('#asset-app-summarypre3', '#asset-app-summary-3');">Read
							More � </a>
					</div>
				</span>
				<div class="asset-more">
					<div id="asset-app-summary-3" class="asset-summary"
						style="display: none;"><%=content3%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>
<c:if test="${not empty applicationName4Title}">
	<!--  THIRD  -->
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${application4Url}"><img alt="" src="${icondocURL}">
				${applicationName4Title}</a>
		</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<span id="asset-app-summarypre4"> <%
 	if (content4.length() > 225)
 		out.println(HtmlUtil.stripHtml(content4.substring(0, 222) + " ..."));
 %>
					<div>
						<a
							href="javascript:switchView('#asset-app-summarypre4', '#asset-app-summary-4');">Read
							More � </a>
					</div>
				</span>
				<div class="asset-more">
					<div id="asset-app-summary-4" class="asset-summary"
						style="display: none;"><%=content4%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>
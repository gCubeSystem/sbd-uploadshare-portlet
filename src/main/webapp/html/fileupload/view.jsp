<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>

<portlet:defineObjects />

<script src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery.uploadfile.min.js"></script>


<%-- <portlet:actionURL var="uploadFileURL" name="uploadFiles" /> --%>
<portlet:resourceURL var="uploadFileURL" id="uploadFiles" />

<script>
  $(document).ready(function(){
	  $("#multipleupload").uploadFile({
	      url:"<%=uploadFileURL.toString()%>",
			multiple : true,
			dragDrop : true,
			sequential:true,
			sequentialCount:1,
			maxFileCount:5,
			fileName : "myfile",
			onSuccess:function(files,data,xhr,pd) {
			    console.log(data);
			}			
		});
	});
</script>

<div id="multipleupload">Upload</div>
<%@include file="init.jsp"%>

<liferay-portlet:actionURL portletConfiguration="true"
	var="configurationURL" />

Here you can customise the icon to show besides the Exploratory name

<%
	String icondocURL = GetterUtil.getString(portletPreferences.getValue("icondocURL", StringPool.BLANK));

	String exploratoryName1Title = GetterUtil.getString(portletPreferences.getValue("exploratoryName1-Title", StringPool.BLANK));
	String exploratoryName2Title = GetterUtil.getString(portletPreferences.getValue("exploratoryName2-Title", StringPool.BLANK));
	String exploratoryName3Title = GetterUtil.getString(portletPreferences.getValue("exploratoryName3-Title", StringPool.BLANK));
	String exploratoryName4Title = GetterUtil.getString(portletPreferences.getValue("exploratoryName4-Title", StringPool.BLANK));
	String exploratoryName5Title = GetterUtil.getString(portletPreferences.getValue("exploratoryName5-Title", StringPool.BLANK));
	String exploratoryName6Title = GetterUtil.getString(portletPreferences.getValue("exploratoryName6-Title", StringPool.BLANK));

	long exploratoryName1DocumentId = GetterUtil.getLong(portletPreferences.getValue("exploratoryName1-DocumentId", StringPool.BLANK));
	long exploratoryName2DocumentId = GetterUtil.getLong(portletPreferences.getValue("exploratoryName2-DocumentId", StringPool.BLANK));
	long exploratoryName3DocumentId = GetterUtil.getLong(portletPreferences.getValue("exploratoryName3-DocumentId", StringPool.BLANK));
	long exploratoryName4DocumentId = GetterUtil.getLong(portletPreferences.getValue("exploratoryName4-DocumentId", StringPool.BLANK));
	long exploratoryName5DocumentId = GetterUtil.getLong(portletPreferences.getValue("exploratoryName5-DocumentId", StringPool.BLANK));
	long exploratoryName6DocumentId = GetterUtil.getLong(portletPreferences.getValue("exploratoryName6-DocumentId", StringPool.BLANK));

	
	String exploratory1Url = GetterUtil.getString(portletPreferences.getValue("exploratory1-Url", StringPool.BLANK));
	String exploratory2Url = GetterUtil.getString(portletPreferences.getValue("exploratory2-Url", StringPool.BLANK));
	String exploratory3Url = GetterUtil.getString(portletPreferences.getValue("exploratory3-Url", StringPool.BLANK));
	String exploratory4Url = GetterUtil.getString(portletPreferences.getValue("exploratory4-Url", StringPool.BLANK));
	String exploratory5Url = GetterUtil.getString(portletPreferences.getValue("exploratory5-Url", StringPool.BLANK));
	String exploratory6Url = GetterUtil.getString(portletPreferences.getValue("exploratory6-Url", StringPool.BLANK));

	
	String displayName_cfg = "";
%>

<aui:form action="<%=configurationURL%>" method="post" name="fm">
	<aui:input name="<%=Constants.CMD%>" type="hidden"
		value="<%=Constants.UPDATE%>" />
	<aui:input name="preferences--icondocURL--" type="text"
		cssClass="text long-field" showRequiredLabel="true" label="Icon docId"
		inlineField="true" inlineLabel="left"
		placeholder="Relative URL of the image"
		helpMessage="Relative URL of the image taken from Liferay CMS of the icons"
		value="<%=icondocURL%>" required="false">

	</aui:input>
	<p>Enter the Exploratory in the following:</p>
	<!-- Application URL -->
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
				<td><aui:input name="preferences--exploratoryName1-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="Exploratory Name 1" inlineField="true" inlineLabel="left"
						placeholder="Exploratory"
						helpMessage="Display Name of the Exploratory"
						value="<%=exploratoryName1Title%>" required="false" /></td>
				<td><aui:input
						name="preferences--exploratoryName1-DocumentId--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="Exploratory Document Id 1" inlineField="true"
						inlineLabel="left" placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=exploratoryName1DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--exploratory1-Url--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 1" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=exploratory1Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
				<td><aui:input name="preferences--exploratoryName2-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="Exploratory Name 2" inlineField="true" inlineLabel="left"
						placeholder="Exploratory"
						helpMessage="Display Name of the Exploratory"
						value="<%=exploratoryName2Title%>" required="false" /></td>
				<td><aui:input
						name="preferences--exploratoryName2-DocumentId--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="Exploratory Document Id 2" inlineField="true"
						inlineLabel="left" placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=exploratoryName2DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--exploratory2-Url--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 2" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=exploratory2Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
				<td><aui:input name="preferences--exploratoryName3-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="Exploratory Name 3" inlineField="true" inlineLabel="left"
						placeholder="Exploratory"
						helpMessage="Display Name of the Exploratory"
						value="<%=exploratoryName3Title%>" required="false" /></td>
				<td><aui:input
						name="preferences--exploratoryName3-DocumentId--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="Exploratory Document Id 3" inlineField="true"
						inlineLabel="left" placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=exploratoryName3DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--exploratory3-Url--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 3" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=exploratory3Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
				<td><aui:input name="preferences--exploratoryName4-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="Exploratory Name 4" inlineField="true" inlineLabel="left"
						placeholder="Exploratory"
						helpMessage="Display Name of the Exploratory"
						value="<%=exploratoryName4Title%>" required="false" /></td>
				<td><aui:input
						name="preferences--exploratoryName4-DocumentId--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="Exploratory Document Id 4" inlineField="true"
						inlineLabel="left" placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=exploratoryName4DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--exploratory4-Url--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 4" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=exploratory4Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
				<td><aui:input name="preferences--exploratoryName5-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="Exploratory Name 5" inlineField="true" inlineLabel="left"
						placeholder="Exploratory"
						helpMessage="Display Name of the Exploratory"
						value="<%=exploratoryName5Title%>" required="false" /></td>
				<td><aui:input
						name="preferences--exploratoryName5-DocumentId--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="Exploratory Document Id 5" inlineField="true"
						inlineLabel="left" placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=exploratoryName5DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--exploratory5-Url--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 5" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=exploratory5Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
				<td><aui:input name="preferences--exploratoryName6-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="Exploratory Name 6" inlineField="true" inlineLabel="left"
						placeholder="Exploratory"
						helpMessage="Display Name of the Exploratory"
						value="<%=exploratoryName6Title%>" required="false" /></td>
				<td><aui:input
						name="preferences--exploratoryName6-DocumentId--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="Exploratory Document Id 6" inlineField="true"
						inlineLabel="left" placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=exploratoryName6DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--exploratory6-Url--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 6" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=exploratory6Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>
</aui:form>
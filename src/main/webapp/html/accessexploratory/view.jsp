<%@include file="init.jsp"%>
<%@ page import="org.gcube.portlets.user.sbd.Utils" %>
<%@ page import="com.liferay.portal.model.User" %>
<%@ page import="com.liferay.portal.util.PortalUtil" %>

<%
	long groupId = themeDisplay.getLayout().getGroupId();
	User currentUser = PortalUtil.getUser( request );

	pageContext.setAttribute("icondocURL",
			GetterUtil.getString(portletPreferences.getValue("icondocURL", StringPool.BLANK)));

	pageContext.setAttribute("exploratoryName1Title",
			GetterUtil.getString(portletPreferences.getValue("exploratoryName1-Title", StringPool.BLANK)));
	pageContext.setAttribute("exploratoryName2Title",
			GetterUtil.getString(portletPreferences.getValue("exploratoryName2-Title", StringPool.BLANK)));
	pageContext.setAttribute("exploratoryName3Title",
			GetterUtil.getString(portletPreferences.getValue("exploratoryName3-Title", StringPool.BLANK)));
	pageContext.setAttribute("exploratoryName4Title",
			GetterUtil.getString(portletPreferences.getValue("exploratoryName4-Title", StringPool.BLANK)));
	pageContext.setAttribute("exploratoryName5Title",
			GetterUtil.getString(portletPreferences.getValue("exploratoryName5-Title", StringPool.BLANK)));
	pageContext.setAttribute("exploratoryName6Title",
			GetterUtil.getString(portletPreferences.getValue("exploratoryName6-Title", StringPool.BLANK)));

	long exploratoryName1DocumentId = GetterUtil
			.getLong(portletPreferences.getValue("exploratoryName1-DocumentId", StringPool.BLANK));
	long exploratoryName2DocumentId = GetterUtil
			.getLong(portletPreferences.getValue("exploratoryName2-DocumentId", StringPool.BLANK));
	long exploratoryName3DocumentId = GetterUtil
			.getLong(portletPreferences.getValue("exploratoryName3-DocumentId", StringPool.BLANK));
	long exploratoryName4DocumentId = GetterUtil
			.getLong(portletPreferences.getValue("exploratoryName4-DocumentId", StringPool.BLANK));
	long exploratoryName5DocumentId = GetterUtil
			.getLong(portletPreferences.getValue("exploratoryName5-DocumentId", StringPool.BLANK));
	long exploratoryName6DocumentId = GetterUtil
			.getLong(portletPreferences.getValue("exploratoryName6-DocumentId", StringPool.BLANK));

	String url1 = GetterUtil.getString(portletPreferences.getValue("exploratory1-Url", StringPool.BLANK));
	pageContext.setAttribute("exploratory1Url", Utils.getVREFriendlyURL(currentUser, url1));

	String url2 = GetterUtil.getString(portletPreferences.getValue("exploratory2-Url", StringPool.BLANK));	
	pageContext.setAttribute("exploratory2Url", Utils.getVREFriendlyURL(currentUser, url2));
	
	String url3 = GetterUtil.getString(portletPreferences.getValue("exploratory3-Url", StringPool.BLANK));	
	pageContext.setAttribute("exploratory3Url", Utils.getVREFriendlyURL(currentUser, url3));
	
	String url4 = GetterUtil.getString(portletPreferences.getValue("exploratory4-Url", StringPool.BLANK));	
	pageContext.setAttribute("exploratory4Url", Utils.getVREFriendlyURL(currentUser, url4));
	
	String url5 = GetterUtil.getString(portletPreferences.getValue("exploratory5-Url", StringPool.BLANK));	
	pageContext.setAttribute("exploratory5Url", Utils.getVREFriendlyURL(currentUser, url5));
	
	String url6 = GetterUtil.getString(portletPreferences.getValue("exploratory6-Url", StringPool.BLANK));	
	pageContext.setAttribute("exploratory6Url", Utils.getVREFriendlyURL(currentUser, url6));
	

	String content1 = "";
	if (exploratoryName1DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + exploratoryName1DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content1 = node.getText();
	}

	String content2 = "";
	if (exploratoryName2DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + exploratoryName2DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content2 = node.getText();
	}

	String content3 = "";
	if (exploratoryName3DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + exploratoryName3DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content3 = node.getText();
	}
	String content4 = "";
	if (exploratoryName4DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + exploratoryName4DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content4 = node.getText();
	}
	String content5 = "";
	if (exploratoryName5DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + exploratoryName5DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content5 = node.getText();
	}
	
	String content6 = "";
	if (exploratoryName6DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + exploratoryName6DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content6 = node.getText();
	}
%>
<c:if test="${not empty exploratoryName1Title}">
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${exploratory1Url}"><img alt="" src="${icondocURL}">
				${exploratoryName1Title}</a>
		</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<span id="asset-summarypre1"> <%
 	if (content1.length() > 225)
 			out.println(HtmlUtil.stripHtml(content1.substring(0, 222) + " ..."));
 %>
					<div>
						<a
							href="javascript:switchView('#asset-summarypre1', '#asset-summary-1');">Read
							More � </a>
					</div>
				</span>
				<div class="asset-more">
					<div id="asset-summary-1" class="asset-summary"
						style="display: none;"><%=content1%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>
<!--  SECOND  -->
<c:if test="${not empty exploratoryName2Title}">
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${exploratory2Url}"><img alt="" src="${icondocURL}">
				${exploratoryName2Title}</a>
		</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<span id="asset-summarypre2"> <%
 	if (content2.length() > 225)
 			out.println(HtmlUtil.stripHtml(content2.substring(0, 222) + " ..."));
 %>
					<div>
						<a
							href="javascript:switchView('#asset-summarypre2', '#asset-summary-2');">Read
							More � </a>
					</div>
				</span>
				<div class="asset-more">
					<div id="asset-summary-2" class="asset-summary"
						style="display: none;"><%=content2%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>
<!--  THIRD  -->
<c:if test="${not empty exploratoryName3Title}">
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${exploratory3Url}"><img alt="" src="${icondocURL}">
				${exploratoryName3Title}</a>
		</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<span id="asset-summarypre3"> <%
 	if (content3.length() > 225)
 			out.println(HtmlUtil.stripHtml(content3.substring(0, 222) + " ..."));
 %>
					<div>
						<a
							href="javascript:switchView('#asset-summarypre3', '#asset-summary-3');">Read
							More � </a>
					</div>
				</span>
				<div class="asset-more">
					<div id="asset-summary-3" class="asset-summary"
						style="display: none;"><%=content3%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>
<!--  FOURTH  -->
<c:if test="${not empty exploratoryName4Title}">
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${exploratory4Url}"><img alt="" src="${icondocURL}">
				${exploratoryName4Title}</a>
		</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<span id="asset-summarypre4"> <%
 	if (content4.length() > 225)
 			out.println(HtmlUtil.stripHtml(content4.substring(0, 222) + " ..."));
 %>
					<div>
						<a
							href="javascript:switchView('#asset-summarypre4', '#asset-summary-4');">Read
							More � </a>
					</div>
				</span>
				<div class="asset-more">
					<div id="asset-summary-4" class="asset-summary"
						style="display: none;"><%=content4%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>
<!--  FIFTH  -->
<c:if test="${not empty exploratoryName5Title}">
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${exploratory5Url}"><img alt="" src="${icondocURL}">
				${exploratoryName5Title}</a>
		</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<span id="asset-summarypre5"> <%
 	if (content5.length() > 225)
 			out.println(HtmlUtil.stripHtml(content5.substring(0, 222) + " ..."));
 %>
					<div>
						<a
							href="javascript:switchView('#asset-summarypre5', '#asset-summary-5');">Read
							More � </a>
					</div>
				</span>
				<div class="asset-more">
					<div id="asset-summary-5" class="asset-summary"
						style="display: none;"><%=content5%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>
<!--  SIXTH  -->
<c:if test="${not empty exploratoryName6Title}">
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${exploratory6Url}"><img alt="" src="${icondocURL}">
				${exploratoryName6Title}</a>
		</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<span id="asset-summarypre6"> <%
 	if (content6.length() > 225)
 			out.println(HtmlUtil.stripHtml(content6.substring(0, 222) + " ..."));
 %>
					<div>
						<a
							href="javascript:switchView('#asset-summarypre6', '#asset-summary-6');">Read
							More � </a>
					</div>
				</span>
				<div class="asset-more">
					<div id="asset-summary-6" class="asset-summary"
						style="display: none;"><%=content6%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>
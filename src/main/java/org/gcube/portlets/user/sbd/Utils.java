package org.gcube.portlets.user.sbd;

import org.gcube.common.portal.GCubePortalConstants;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.GroupLocalServiceUtil;

public class Utils {
	private static Log _log = LogFactoryUtil.getLog(Utils.class);
	/**
	 * 
	 * @param currentUser
	 * @param urlContainingGroupId
	 * @return the direct VRE Url if the member belongs to the VRE, the page to ask for registration on this VRE othwerise
	 */
	public static String getVREFriendlyURL(User currentUser, String urlContainingGroupId) {
		String[] splits = urlContainingGroupId.split("=");
		long groupId = -1;
		try {
			if (splits.length > 0) {
				groupId = Long.parseLong(splits[1]);
				long[] userGroupIds = currentUser.getGroupIds();
				for (int i = 0; i < userGroupIds.length; i++) {
					if (groupId == userGroupIds[i])
						return GCubePortalConstants.PREFIX_GROUP_URL +GroupLocalServiceUtil.getGroup(groupId).getFriendlyURL();
				}
			} 
			return urlContainingGroupId;
		} catch (Exception e) {
			_log.error("Something is wrong in the url passed: " + urlContainingGroupId);
			return urlContainingGroupId;
		}
	}
}

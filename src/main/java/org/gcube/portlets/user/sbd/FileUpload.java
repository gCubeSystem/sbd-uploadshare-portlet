package org.gcube.portlets.user.sbd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.upload.FileItem;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;



/**
 * Portlet implementation class FileUpload
 */
public class FileUpload extends MVCPortlet {
	private final static String UPLOADED_FILE_ATTR_NAME = "myfile";
	//make sure in the jsp there's <portlet:resourceURL var="uploadFileURL" id="uploadFiles" />
	private final static String RESOURCE_URL_ID = "uploadFiles";
	

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws PortletException, IOException {            
		String resourceID = resourceRequest.getResourceID();
		if (resourceID.equals(RESOURCE_URL_ID)) {
			System.out.println("here test uploadFiles");
			UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
			File multipartFile = uploadRequest.getFile(UPLOADED_FILE_ATTR_NAME);

			FileItem[] items = uploadRequest.getMultipartParameterMap().get(UPLOADED_FILE_ATTR_NAME);
			String fileName = "";
			String contentType = "";
			for (int i = 0; i < items.length; i++) {
				fileName = items[i].getFileName();
				contentType = items[i].getContentType();
				System.out.println(fileName);
				System.out.println(contentType);
			}
			Path written = Files.write(Paths.get(System.getProperty("catalina.home")+"/temp/"+fileName),  FileUtil.getBytes(multipartFile));
			System.out.println("Written File = " + written.toRealPath());
			JSONObject fileObject = JSONFactoryUtil.createJSONObject();
			fileObject.put("fileid", "12345");
			resourceResponse.getWriter().println(fileObject);	
			
		} else {
			System.out.println("it's another method");
		}
	}

}

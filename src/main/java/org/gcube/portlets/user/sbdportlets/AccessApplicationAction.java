package org.gcube.portlets.user.sbdportlets;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;

public class AccessApplicationAction extends DefaultConfigurationAction {
	private static Log _log = LogFactoryUtil.getLog(AccessApplicationAction.class);
	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {      	  
		super.processAction(portletConfig, actionRequest, actionResponse);
		PortletPreferences prefs = actionRequest.getPreferences();
		String icondocURL = prefs.getValue("icondocURL", "0");     
		_log.info("icondocURL = " + icondocURL + " in ConfigurationAction.processAction() saved correctly");

		String applicationName1Title = prefs.getValue("applicationName1-Title", "");     
		_log.info("applicationName1-Title = " + applicationName1Title + " in ConfigurationAction.processAction() saved correctly");
		String applicationName2Title = prefs.getValue("applicationName2-Title", "");     
		_log.info("applicationName2-Title = " + applicationName2Title + " in ConfigurationAction.processAction() saved correctly");
		String applicationName3Title = prefs.getValue("applicationName3-Title", "");     
		_log.info("applicationName3-Title = " + applicationName3Title + " in ConfigurationAction.processAction() saved correctly");
		String applicationName4Title = prefs.getValue("applicationName4-Title", "");     
		_log.info("applicationName4-Title = " + applicationName4Title + " in ConfigurationAction.processAction() saved correctly");

		long applicationName1DocumentId =  Long.parseLong(prefs.getValue("applicationName1-DocumentId", "0"));     
		_log.info("applicationName1-DocumentId = " + applicationName1DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long applicationName2DocumentId =  Long.parseLong(prefs.getValue("applicationName2-DocumentId", "0"));     
		_log.info("applicationName2-DocumentId = " + applicationName2DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long applicationName3DocumentId =  Long.parseLong(prefs.getValue("applicationName3-DocumentId", "0"));     
		_log.info("applicationName3-DocumentId = " + applicationName3DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long applicationName4DocumentId =  Long.parseLong(prefs.getValue("applicationName4-DocumentId", "0"));     
		_log.info("applicationName4-DocumentId = " + applicationName4DocumentId + " in ConfigurationAction.processAction() saved correctly");
		
		String application1Url = prefs.getValue("application1-Url", "");     
		_log.info("application1-Url = " + application1Url + " in ConfigurationAction.processAction() saved correctly");
		String application2Url = prefs.getValue("application2-Url", "");     
		_log.info("application2-Url = " + application2Url + " in ConfigurationAction.processAction() saved correctly");
		String application3Url = prefs.getValue("application3-Url", "");     
		_log.info("application3-Url = " + application3Url + " in ConfigurationAction.processAction() saved correctly");
		String application4Url = prefs.getValue("application4-Url", "");     
		_log.info("application4-Url = " + application4Url + " in ConfigurationAction.processAction() saved correctly");
	

	}

	@Override
	public String render(PortletConfig portletConfig,
			RenderRequest renderRequest, RenderResponse renderResponse)
					throws Exception {
		return "/html/accessapplication/config.jsp";
	}

}
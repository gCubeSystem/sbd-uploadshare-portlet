package org.gcube.portlets.user.sbdportlets;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;

public class AccessConfigurationAction extends DefaultConfigurationAction {
	private static Log _log = LogFactoryUtil.getLog(AccessConfigurationAction.class);
	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {      	  
		super.processAction(portletConfig, actionRequest, actionResponse);
		PortletPreferences prefs = actionRequest.getPreferences();
		String icondocURL = prefs.getValue("icondocURL", "0");     
		_log.info("icondocURL = " + icondocURL + " in ConfigurationAction.processAction() saved correctly");

		String exploratoryName1Title = prefs.getValue("exploratoryName1-Title", "");     
		_log.info("exploratoryName1-Title = " + exploratoryName1Title + " in ConfigurationAction.processAction() saved correctly");
		String exploratoryName2Title = prefs.getValue("exploratoryName2-Title", "");     
		_log.info("exploratoryName2-Title = " + exploratoryName2Title + " in ConfigurationAction.processAction() saved correctly");
		String exploratoryName3Title = prefs.getValue("exploratoryName3-Title", "");     
		_log.info("exploratoryName3-Title = " + exploratoryName3Title + " in ConfigurationAction.processAction() saved correctly");
		String exploratoryName4Title = prefs.getValue("exploratoryName4-Title", "");     
		_log.info("exploratoryName4-Title = " + exploratoryName4Title + " in ConfigurationAction.processAction() saved correctly");
		String exploratoryName5Title = prefs.getValue("exploratoryName5-Title", "");     
		_log.info("exploratoryName5-Title = " + exploratoryName5Title + " in ConfigurationAction.processAction() saved correctly");

		
		long exploratoryName1DocumentId =  Long.parseLong(prefs.getValue("exploratoryName1-DocumentId", "0"));     
		_log.info("exploratoryName1-DocumentId = " + exploratoryName1DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long exploratoryName2DocumentId =  Long.parseLong(prefs.getValue("exploratoryName2-DocumentId", "0"));     
		_log.info("exploratoryName2-DocumentId = " + exploratoryName2DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long exploratoryName3DocumentId =  Long.parseLong(prefs.getValue("exploratoryName3-DocumentId", "0"));     
		_log.info("exploratoryName3-DocumentId = " + exploratoryName3DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long exploratoryName4DocumentId =  Long.parseLong(prefs.getValue("exploratoryName4-DocumentId", "0"));     
		_log.info("exploratoryName4-DocumentId = " + exploratoryName4DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long exploratoryName5DocumentId =  Long.parseLong(prefs.getValue("exploratoryName5-DocumentId", "0"));     
		_log.info("exploratoryName4-DocumentId = " + exploratoryName5DocumentId + " in ConfigurationAction.processAction() saved correctly");

		
		String exploratory1Url = prefs.getValue("exploratory1-Url", "");     
		_log.info("exploratory1-Url = " + exploratory1Url + " in ConfigurationAction.processAction() saved correctly");
		String exploratory2Url = prefs.getValue("exploratory2-Url", "");     
		_log.info("exploratory2-Url = " + exploratory2Url + " in ConfigurationAction.processAction() saved correctly");
		String exploratory3Url = prefs.getValue("exploratory3-Url", "");     
		_log.info("exploratory3-Url = " + exploratory3Url + " in ConfigurationAction.processAction() saved correctly");
		String exploratory4Url = prefs.getValue("exploratory4-Url", "");     
		_log.info("exploratory4-Url = " + exploratory4Url + " in ConfigurationAction.processAction() saved correctly");
		String exploratory5Url = prefs.getValue("exploratory5-Url", "");     
		_log.info("exploratory5-Url = " + exploratory5Url + " in ConfigurationAction.processAction() saved correctly");
	

	}

	@Override
	public String render(PortletConfig portletConfig,
			RenderRequest renderRequest, RenderResponse renderResponse)
					throws Exception {
		return "/html/accessexploratory/config.jsp";
	}

}